$.get(chrome.extension.getURL('visual/config.css'), 
	function(data) {
		var script = document.createElement("link");
		script.setAttribute("rel", "stylesheet");
		script.setAttribute("href", "chrome-extension://hljboickkddpbainofdaagkcgdmajnbi/visual/config.css");
		script.innerHTML = data;
		document.body.appendChild(script);
	}
);

$.get(chrome.extension.getURL('js/main.js'), 
	function(data) {
		var script = document.createElement("script");
		script.setAttribute("type", "text/javascript");
		script.innerHTML = data;
		document.getElementsByTagName("head")[0].appendChild(script);
	}
);